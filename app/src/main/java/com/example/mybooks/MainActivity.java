package com.example.mybooks;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

public class MainActivity extends AppCompatActivity {

    private FirebaseAuth firebaseAuth;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        firebaseAuth = FirebaseAuth.getInstance();

        //démarrer HomeActivity après un délai
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                startActivity(new Intent( MainActivity.this, HomeActivity.class));
                finish(); //finir cette activité
                //checkUser();
            }
        }, 2000); //2000 signifie 2s
    }

    private void checkUser() {
        //get current user, if logged in
        FirebaseUser firebaseUser = firebaseAuth.getCurrentUser();
        if (firebaseUser == null){
            //démarrer HomeActivity
            startActivity(new Intent( MainActivity.this, HomeActivity.class));
            finish(); //finir cette activité
        }
        else {
            //check in db
            DatabaseReference ref = FirebaseDatabase.getInstance().getReference("Users");
            ref.child(firebaseUser.getUid())
                    .addListenerForSingleValueEvent(new ValueEventListener() {
                        @Override
                        public void onDataChange( DataSnapshot snapshot) {
                            //get user type
                            String userType = ""+snapshot.child("userType").getValue();

                            //check user type
                            if (userType.equals("user")){
                                //open user dashboard
                                startActivity(new Intent(MainActivity.this, DashboardUserActivity.class));
                                finish();
                            }
                            else if (userType.equals("admin")){
                                //open dashboard admin
                            }
                        }

                        @Override
                        public void onCancelled( DatabaseError error) {

                        }
                    });
        }
    }
}