package com.example.mybooks;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Patterns;
import android.view.View;
import android.widget.Toast;

import com.example.mybooks.databinding.ActivityRegisterBinding;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.HashMap;

public class RegisterActivity extends AppCompatActivity {

    private ActivityRegisterBinding binding;

    private FirebaseAuth firebaseAuth;

    private ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityRegisterBinding.inflate(getLayoutInflater());
        //setContentView(R.layout.activity_register);
        setContentView(binding.getRoot());

        firebaseAuth = FirebaseAuth.getInstance();

        //setup progress dialog
        progressDialog = new ProgressDialog(this);
        progressDialog.setTitle("Patientez s'il vous plait");
        progressDialog.setCanceledOnTouchOutside(false);

        //click sur le bouton de création d'un compte
        binding.registerBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                validateData();
            }
        });


        //click sur le bouton de retour
        binding.backBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
    }

    private String name = "", firstname = "", tel = "", email = "", password = "";
    private void validateData() {

        //get data
        name= binding.nameEdit.getText().toString().trim();
        firstname= binding.firstNameEdit.getText().toString().trim();
        tel= binding.phoneEdit.getText().toString().trim();
        email= binding.emailEdit.getText().toString().trim();
        password= binding.passwordEdit.getText().toString().trim();
        String cpassword= binding.passwordConfirm.getText().toString().trim();

        //validate data
        if (TextUtils.isEmpty(name)){
            Toast.makeText(this, "Entrez votre nom...", Toast.LENGTH_SHORT).show();
        }
        else if (!Patterns.EMAIL_ADDRESS.matcher(email).matches()){
            Toast.makeText(this, "Email invalide...", Toast.LENGTH_SHORT).show();
        }
        else if (TextUtils.isEmpty(firstname)){
            Toast.makeText(this, "Entrez votre prénom...", Toast.LENGTH_SHORT).show();
        }
        else if (TextUtils.isEmpty(tel)){
            Toast.makeText(this, "Entrez votre numéro de téléphone...", Toast.LENGTH_SHORT).show();
        }
        else if (TextUtils.isEmpty(password)){
            Toast.makeText(this, "Entrez un mot de passe...", Toast.LENGTH_SHORT).show();
        }
        else if (TextUtils.isEmpty(cpassword)){
            Toast.makeText(this, "Confirmez votre mot de passe...", Toast.LENGTH_SHORT).show();
        }
        else if (!password.equals(cpassword)){
            Toast.makeText(this, "Mot de passe non conforme...", Toast.LENGTH_SHORT).show();
        }
        else {
            createUserAccount();
        }
    }

    private void createUserAccount() {
        //show progress
        progressDialog.setMessage("Création du compte...");
        progressDialog.show();

        //create user in firebase auth
        firebaseAuth.createUserWithEmailAndPassword(email, password)
                .addOnSuccessListener(new OnSuccessListener<AuthResult>() {
                    @Override
                    public void onSuccess(AuthResult authResult) {
                        //sucess create account
                        upDateUser();
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(Exception e) {
                        //failed create account
                        progressDialog.dismiss();
                        Toast.makeText(RegisterActivity.this, ""+e.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                });
    }

    private void upDateUser() {
        progressDialog.setMessage("Enregistrement des informations...");

        //timestamp
        long timestamp = System.currentTimeMillis();

        //get current user uid, since user is registered so we can get now
        String uid = firebaseAuth.getUid();

        //setup data to add in db
        HashMap<String, Object> hashMap = new HashMap<>();
        hashMap.put("uid", uid);
        hashMap.put("email", email);
        hashMap.put("name", name);
        hashMap.put("firstname", firstname);
        hashMap.put("phone", tel);
        hashMap.put("profileImage", "");
        hashMap.put("userType", "user");//possible values are user, admin: will make admin manually in firebase realtime db by changing this value
        hashMap.put("timestamp", timestamp);

        //set data to db
        DatabaseReference ref = FirebaseDatabase.getInstance().getReference("Users");
        ref.child(uid)
                .setValue(hashMap)
                .addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void unused) {
                        //data add to db
                        progressDialog.dismiss();
                        Toast.makeText(RegisterActivity.this, "Compte créé avec succès...", Toast.LENGTH_SHORT).show();

                        //since user account is created so start dashboard of user
                        startActivity(new Intent(RegisterActivity.this, DashboardUserActivity.class));
                        finish();
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure( Exception e) {
                        //data failed adding to db
                        progressDialog.dismiss();
                        Toast.makeText(RegisterActivity.this, ""+e.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                });
    }
}